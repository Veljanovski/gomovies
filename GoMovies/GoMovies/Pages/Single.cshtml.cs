using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Neo4j.Driver;
using GoMovies.Models;

namespace GoMovies.Pages
{
    public class SingleModel : PageModel
    {
        private readonly IDriver _driver;
        [BindProperty]
        public int NewRating { get; set; }
        [BindProperty]
        public Movie Movie { get; set; }
        public Director Director { get; set; }
        public Actor Actor { get; set; }
        [BindProperty]
        public Comment Comment { get; set; }
        public IList<Comment> comments { get; set; }
        public SingleModel(IDriver driver)
        {
            _driver = driver;
            Movie = new Movie();
            Director = new Director();
            Actor = new Actor();
            comments = new List<Comment>();
        }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            IResultCursor cursor;
            var list = new List<INode>();
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync($"MATCH (m:Movie) WHERE id(m)={id} RETURN m");
                var node = await cursor.SingleAsync(r => r["m"].As<INode>());

                Movie.id = (int)node.Id;
                Movie.title = node["title"].As<String>();
                Movie.genre = node["genre"].As<String>();
                Movie.year = node["year"].As<int>();
                Movie.rating = node["rating"].As<double>();
                Movie.votes = node["votes"].As<int>();

                cursor = await session.RunAsync($"MATCH (m:Movie)--(d:Director) WHERE id(m)={id} RETURN d");
                node = await cursor.SingleAsync(r => r["d"].As<INode>());
                Director.name = node["name"].As<String>();

                cursor = await session.RunAsync($"MATCH (m:Movie)--(a:Actor) WHERE id(m)={id} RETURN a");
                node = await cursor.SingleAsync(r => r["a"].As<INode>());
                Actor.name = node["name"].As<String>();

                cursor = await session.RunAsync($"MATCH (m:Movie)--(c:Comment) WHERE id(m)={Movie.id} RETURN c");
                list = await cursor.ToListAsync(record =>
                record["c"].As<INode>());
                foreach (var r in list)
                {
                    Comment c = new Comment();
                    c.user = r["user"].As<String>();
                    c.text = r["text"].As<String>();

                    comments.Add(c);
                }
            }
            finally
            {
                await session.CloseAsync();
            }
            return this.Page();
        }

        public async Task<IActionResult> OnPostRateCommentAsync()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }
            IResultCursor cursor;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync($@"MATCH (m:Movie)
                WHERE id(m)={Movie.id}
                RETURN m");
                var node = await cursor.SingleAsync(r => r["m"].As<INode>());

                Movie.id = (int)node.Id;
                Movie.title = node["title"].As<String>();
                Movie.genre = node["genre"].As<String>();
                Movie.year = node["year"].As<int>();
                Movie.rating = node["rating"].As<double>();
                Movie.votes = node["votes"].As<int>();

                double temp = Movie.rating * Movie.votes;
                temp += NewRating;
                Movie.rating = temp / (++Movie.votes);

                cursor = await session.RunAsync(@$"CREATE (c:Comment {{user: '{Comment.user}', text: '{Comment.text}'}}) RETURN c");
                node = await cursor.SingleAsync(r => r["c"].As<INode>());
                int cid = (int)node.Id;

                cursor = await session.RunAsync($@"MATCH (c:Comment) WHERE id(c)={cid}
                MATCH (m:Movie) WHERE id(m)={Movie.id} SET m.rating = '{Movie.rating}' SET m.votes = '{Movie.votes}'
                MERGE (c)-[:COMMENTED_ON]->(m)");
            }
            finally
            {
                await session.CloseAsync();
            }
            return Redirect("Single?id=" + Movie.id);
        }

        public async Task<IActionResult> OnPostDeleteMovieAsync()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }
            IResultCursor cursor;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync($@"MATCH (m:Movie)--(c:Comment)
                WHERE id(m)={Movie.id}
                DETACH DELETE c
                DETACH DELETE m");
            }
            finally
            {
                await session.CloseAsync();
            }
            return RedirectToPage("Index");
        }
    }
}
