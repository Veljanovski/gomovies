﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoMovies.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace GoMovies.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IDriver _driver;

        private readonly ILogger<IndexModel> _logger;
        [BindProperty]
        public Movie Movie { get; set; }
        [BindProperty]
        public Director Director { get; set; }
        [BindProperty]
        public Actor Actor { get; set; }
        public IList<Movie> topMovies { get; set; }
        public IList<Movie> latestMovies { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IDriver driver)
        {
            _logger = logger;
            _driver = driver;
            topMovies = new List<Movie>();
            latestMovies = new List<Movie>();
        }

        public async Task OnGetAsync()
        {
            var list = new List<INode>();
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                var cursor = await session.RunAsync(@"MATCH (m:Movie) RETURN m ORDER BY m.year DESC limit 10");
                list = await cursor.ToListAsync(record =>
                record["m"].As<INode>());
                foreach (var r in list)
                {
                    Movie m = new Movie();
                    m.id = (int)r.Id;
                    m.title = r["title"].As<String>();
                    m.genre = r["genre"].As<String>();
                    m.year = r["year"].As<int>();
                    m.rating = r["rating"].As<double>();
                    m.votes = r["votes"].As<int>();
                    
                    latestMovies.Add(m);
                }

                cursor = await session.RunAsync(@"MATCH (m:Movie) RETURN m ORDER BY m.rating DESC limit 10");
                list = await cursor.ToListAsync(record =>
                record["m"].As<INode>());
                foreach (var r in list)
                {
                    Movie m = new Movie();
                    m.id = (int)r.Id;
                    m.title = r["title"].As<String>();
                    m.genre = r["genre"].As<String>();
                    m.year = r["year"].As<int>();
                    m.rating = r["rating"].As<double>();
                    m.votes = r["votes"].As<int>();

                    topMovies.Add(m);
                }
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        public async Task<IActionResult> OnPostAddMovieAsync()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }
            IResultCursor cursor;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync(@$"MATCH (m:Movie) 
                WHERE m.title='{Movie.title}'AND m.genre='{Movie.genre}' AND m.year='{Movie.year}' 
                RETURN m");
                var node = await cursor.FetchAsync();
                if (node == false)
                {
                    cursor = await session.RunAsync(@$"MATCH (d:Director) WHERE d.name='{Director.name}' RETURN d");
                    var dnode = await cursor.FetchAsync();
                    if (dnode == false)
                        cursor = await session.RunAsync($"CREATE (d:Director {{name:'{Director.name}'}})");

                    cursor = await session.RunAsync(@$"MATCH (a:Actor) WHERE a.name='{Actor.name}' RETURN a");
                    var anode = await cursor.FetchAsync();
                    if (anode == false)
                        cursor = await session.RunAsync($"CREATE (a:Actor {{name:'{Actor.name}'}})");

                    cursor = await session.RunAsync(@$"CREATE (m:Movie {{title: '{Movie.title}', genre: '{Movie.genre}', year: '{Movie.year}', 
                    rating: '0', votes: '0'}})");

                    cursor = await session.RunAsync(@$"MATCH (d:Director) WHERE d.name='{Director.name}'
                    MATCH (a:Actor) WHERE a.name='{Actor.name}'
                    MATCH (m:Movie) WHERE m.title='{Movie.title}' AND m.genre='{Movie.genre}' AND m.year='{Movie.year}'
                    MERGE (a)-[:ACTED_IN]->(m)
                    MERGE (d)-[:DIRECTED]->(m)");
                }                
            }
            finally
            {
                await session.CloseAsync();
            }
            return RedirectToPage("Index");
        }

        public async Task<IActionResult> OnPostAddDirectorAsync()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }
            IResultCursor cursor;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync(@$"MATCH (d:Director) 
                WHERE d.name='{Director.name}'AND d.born='{Director.born}'
                RETURN d");
                var node = await cursor.FetchAsync();
                if (node == false)
                    cursor = await session.RunAsync($"CREATE (d:Director {{name:'{Director.name}', born: '{Director.born}'}})");               
            }
            finally
            {
                await session.CloseAsync();
            }
            return RedirectToPage("Index");
        }

        public async Task<IActionResult> OnPostAddActorAsync()
        {
            if (!this.ModelState.IsValid)
            {
                return this.Page();
            }
            IResultCursor cursor;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync(@$"MATCH (a:Actor) 
                WHERE a.name='{Actor.name}'AND a.born='{Actor.born}' AND a.birthplace='{Actor.birthplace}'
                RETURN a");
                var node = await cursor.FetchAsync();
                if (node == false)
                    cursor = await session.RunAsync($"CREATE (a:Actor {{name:'{Actor.name}', born: '{Actor.born}', birthplace: '{Actor.birthplace}'}})");
            }
            finally
            {
                await session.CloseAsync();
            }
            return RedirectToPage("Index");
        }
    }
}
