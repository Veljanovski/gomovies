using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoMovies.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Neo4j.Driver;

namespace GoMovies.Pages
{
    public class ListModel : PageModel
    {
        private readonly IDriver _driver;
        public Movie Movie { get; set; }
        public Director Director { get; set; }
        public Actor Actor { get; set; }
        public IList<(Movie Movie, Actor Actor, Director Director, int Index)> tuples { get; set; }
        public ListModel(IDriver driver)
        {
            _driver = driver;
            tuples = new List<(Movie, Actor, Director, int)>();
        }

        public async Task<IActionResult> OnGetAsync()
        {
            IResultCursor cursor;
            var list = new List<INode>();
            int i = 0;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync($"MATCH (m:Movie) RETURN m");
                list = await cursor.ToListAsync(r => r["m"].As<INode>());

                foreach (var node in list)
                {
                    Movie = new Movie();
                    Movie.id = (int)node.Id;
                    Movie.title = node["title"].As<String>();
                    Movie.year = node["year"].As<int>();
                    Movie.genre = node["genre"].As<String>();
                    Movie.rating = node["rating"].As<double>();

                    Director = new Director();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(d:Director) WHERE id(m)={Movie.id} RETURN d");
                    var n = await cursor.SingleAsync(r => r["d"].As<INode>());
                    Director.name = n["name"].As<String>();

                    Actor = new Actor();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(a:Actor) WHERE id(m)={Movie.id} RETURN a");
                    n = await cursor.SingleAsync(r => r["a"].As<INode>());
                    Actor.name = n["name"].As<String>();
                    i++;

                    tuples.Add((Movie, Actor, Director, i));
                }
            }
            finally
            {
                await session.CloseAsync();
            }
            return this.Page();
        }

        public async Task<IActionResult> OnGetGenreAsync(String genre)
        {
            IResultCursor cursor;
            var list = new List<INode>();
            int i = 0;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync($"MATCH (m:Movie) WHERE m.genre='{genre}' RETURN m");
                list = await cursor.ToListAsync(r => r["m"].As<INode>());

                foreach (var node in list)
                {
                    Movie = new Movie();
                    Movie.id = (int)node.Id;
                    Movie.title = node["title"].As<String>();
                    Movie.year = node["year"].As<int>();
                    Movie.genre = node["genre"].As<String>();
                    Movie.rating = node["rating"].As<double>();

                    Director = new Director();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(d:Director) WHERE id(m)={Movie.id} RETURN d");
                    var n = await cursor.SingleAsync(r => r["d"].As<INode>());
                    Director.name = n["name"].As<String>();

                    Actor = new Actor();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(a:Actor) WHERE id(m)={Movie.id} RETURN a");
                    n = await cursor.SingleAsync(r => r["a"].As<INode>());
                    Actor.name = n["name"].As<String>();
                    i++;

                    tuples.Add((Movie, Actor, Director, i));
                }
            }
            finally
            {
                await session.CloseAsync();
            }
            return this.Page();
        }

        public async Task<IActionResult> OnGetReleaseAsync(String release)
        {
            IResultCursor cursor;
            var list = new List<INode>();
            int i = 0;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                switch(release)
                {
                    case "2010s":
                        cursor = await session.RunAsync($"MATCH (m:Movie) WHERE m.year>='2010' AND m.year<'2020' RETURN m");
                        break;
                    case "2000s":
                        cursor = await session.RunAsync($"MATCH (m:Movie) WHERE m.year>='2000' AND m.year<'2010' RETURN m");
                        break;
                    case "1990s":
                        cursor = await session.RunAsync($"MATCH (m:Movie) WHERE m.year>='1990' AND m.year<'2000' RETURN m");
                        break;
                    case "1980s":
                        cursor = await session.RunAsync($"MATCH (m:Movie) WHERE m.year>='1980' AND m.year<'1990' RETURN m");
                        break;
                    default:
                        cursor = await session.RunAsync($"MATCH (m:Movie) RETURN m");
                        break;
                }
                list = await cursor.ToListAsync(r => r["m"].As<INode>());

                foreach (var node in list)
                {
                    Movie = new Movie();
                    Movie.id = (int)node.Id;
                    Movie.title = node["title"].As<String>();
                    Movie.year = node["year"].As<int>();
                    Movie.genre = node["genre"].As<String>();
                    Movie.rating = node["rating"].As<double>();

                    Director = new Director();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(d:Director) WHERE id(m)={Movie.id} RETURN d");
                    var n = await cursor.SingleAsync(r => r["d"].As<INode>());
                    Director.name = n["name"].As<String>();

                    Actor = new Actor();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(a:Actor) WHERE id(m)={Movie.id} RETURN a");
                    n = await cursor.SingleAsync(r => r["a"].As<INode>());
                    Actor.name = n["name"].As<String>();
                    i++;

                    tuples.Add((Movie, Actor, Director, i));
                }
            }
            finally
            {
                await session.CloseAsync();
            }
            return this.Page();
        }

        public async Task<IActionResult> OnGetSearchAsync(String search)
        {
            IResultCursor cursor;
            var list = new List<INode>();
            int i = 0;
            IAsyncSession session = _driver.AsyncSession();
            try
            {
                cursor = await session.RunAsync(@$"MATCH (m:Movie)--(a:Actor)
                MATCH (m:Movie)--(d:Director)
                WHERE m.title=~'(?i).*{search}.*'
                OR a.name=~'(?i).*{search}.*'
                OR d.name=~'(?i).*{search}.*'
                RETURN m");
                list = await cursor.ToListAsync(r => r["m"].As<INode>());

                foreach (var node in list)
                {
                    Movie = new Movie();
                    Movie.id = (int)node.Id;
                    Movie.title = node["title"].As<String>();
                    Movie.year = node["year"].As<int>();
                    Movie.genre = node["genre"].As<String>();
                    Movie.rating = node["rating"].As<double>();

                    Director = new Director();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(d:Director) WHERE id(m)={Movie.id} RETURN d");
                    var n = await cursor.SingleAsync(r => r["d"].As<INode>());
                    Director.name = n["name"].As<String>();

                    Actor = new Actor();
                    cursor = await session.RunAsync($"MATCH (m:Movie)--(a:Actor) WHERE id(m)={Movie.id} RETURN a");
                    n = await cursor.SingleAsync(r => r["a"].As<INode>());
                    Actor.name = n["name"].As<String>();
                    i++;

                    tuples.Add((Movie, Actor, Director, i));
                }
            }
            finally
            {
                await session.CloseAsync();
            }
            return this.Page();
        }
    }
}
