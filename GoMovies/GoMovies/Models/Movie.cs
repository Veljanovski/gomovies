﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Neo4j.Driver;

namespace GoMovies.Models
{
    public class Movie
    {
        public int id { get; set; }
        public String title { get; set; }
        public String genre { get; set; }
        public int year { get; set; }
        public double rating { get; set; }
        public int votes { get; set; }
    }   
}
