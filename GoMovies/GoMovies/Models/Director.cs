﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoMovies.Models
{
    public class Director
    {
        //public String id { get; set; }
        public String name { get; set; }
        public int born { get; set; }
    }
}
