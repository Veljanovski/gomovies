﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoMovies.Models
{
    public class Comment
    {
        public String user { get; set; }
        public String text { get; set; }
    }
}
